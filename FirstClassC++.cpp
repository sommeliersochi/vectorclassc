#include <string>
#include <iostream>
#include <cmath>



using namespace std;



class Vector {
public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    

    void Show() {
        cout << "------------------------" << endl;
        cout << " " << x << "\t" << y << "\t" << z << endl;

     }

    void Res() //��������� ����� Vector public �������, ������� ����� ���������� ����� (������) �������.
    {
        int res;
        int arr[3]{ x,y,z };

       res = arr[0] * arr[0] + arr[1] * arr[1] + arr[2] * arr[2];
       res = sqrt(res);
       cout << "\nmodule vector = " << res << endl;

    }

private:
    double x;
    double y;
    double z;
    
};


class Human //������� �����  
{
public: //������� public ����� ��� ������ ���� ������.
    Human(): Age(0), Name( "No_Name" )
    {}

    Human(double _Age, string _Name): Age(_Age), Name(_Name)
    {}

    void ShowPassport() {
        cout << "------------------------" << endl;
        cout << "Name:" << Name << ' ' << "Age:" << Age << endl;
    }

private: //�� ������ �������, �������� ��� ������ private.
    double Age;
    string Name;
};

int main()
{   
    Vector v(2, 2, 2);
    v.Show();
    v.Res(); // 2.��������������
    Vector v2(3, 4, 5);
    v2.Show();
    v2.Res();
    Vector v3(10, 10, 10);
    v3.Show();
    v3.Res();
    Human first(28, "Ilya_Krylov");
    first.ShowPassport(); // 1. �������������� 
    Human second(30, "Artyom_Razzakov"); // test_test :D
    second.ShowPassport();
        
  


}

